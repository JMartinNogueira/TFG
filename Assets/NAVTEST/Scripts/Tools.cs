﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Tools  {

    public static bool sameVertex(Vector3 vertex, Triangle triangle, NavMeshTriangulation mesh)
    {
        bool same = false;
        if (vertex == mesh.vertices[mesh.indices[triangle.index]] || vertex == mesh.vertices[mesh.indices[triangle.index + 1]] || vertex == mesh.vertices[mesh.indices[triangle.index + 2]])
        {
            same = true;
        }
        return same;
    }

    public static bool TriangulationBarycentre( Vector3 A, Vector3 B, Vector3 C, Vector3 P )
    {
        // http://blackpawn.com/texts/pointinpoly/

        //Compute Vectors
        Vector3 v0 = C - A;
        Vector3 v1 = B - A;
        Vector3 v2 = P - A;

        //Compute Dot Products
        float dot00 = Vector3.Dot(v0, v0);
        float dot01 = Vector3.Dot(v0, v1);
        float dot02 = Vector3.Dot(v0, v2);
        float dot11 = Vector3.Dot(v1, v1);
        float dot12 = Vector3.Dot(v1, v2);

        //Compute barycentric coordinates
        float invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
        float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
        float v = (dot00 * dot12 - dot01 * dot02) * invDenom;
            
        //Check
        return (u >= 0) && (v >= 0) && (u + v < 1);
    }

    public static int CurrentTrianglePath( NavMeshTriangulation mesh, List<Triangle> triangleList, Vector3 p )
    {
        //Return position list of the current triangle, -1 if not found;
        int current = 0;
        foreach( Triangle t in triangleList )
        {
            if ( TriangulationBarycentre(mesh.vertices[mesh.indices[t.index]], mesh.vertices[mesh.indices[t.index + 1]], mesh.vertices[mesh.indices[t.index + 2]], p) )
            {
                return current; 
            }
            current++;
        }
        return -1;
    }

}
