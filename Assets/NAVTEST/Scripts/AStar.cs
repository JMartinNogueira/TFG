﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AStar
{

    public List<Triangle> path;
    public List<Triangle> triangleList;
    public Triangle start;
    public Triangle end;
    public NavMeshTriangulation mesh;
    public volatile bool jobDone = false;
    // Use this for initialization

    public AStar(Triangle start, Triangle end, NavMeshTriangulation mesh, List<Triangle> triangleLis)
    {
        this.start = start;
        this.end = end;
        this.mesh = mesh;
        this.triangleList = triangleLis;
    }

    public void FindPath()
    {
        path = FindPathActual(start, end);

        jobDone = true;
    }

    private List<Triangle> FindPathActual(Triangle start, Triangle end)
    {
        List<Triangle> foundPath = new List<Triangle>();
        List<Triangle> openSet = new List<Triangle>();
        HashSet<Triangle> closedSet = new HashSet<Triangle>();

        openSet.Add(start);
        int counter = 0;
        while (openSet.Count > 0)
        {
            
            ++counter;
            Triangle currentTriangle = openSet[0];

            for (int i = 0; i < openSet.Count; i++)
            {
                if (openSet[i].fCost < currentTriangle.fCost || (openSet[i].fCost == currentTriangle.fCost && openSet[i].gCost < currentTriangle.gCost))
                {
                    if (currentTriangle.area != openSet[i].area)
                    {
                        currentTriangle = openSet[i];
                    }
                }
            }
            openSet.Remove(currentTriangle);
            closedSet.Add(currentTriangle);

            if (currentTriangle.Equals(end))
            {
                foundPath = RetracePath(start, currentTriangle);
                break;
            }

            foreach (Triangle neighbour in GetNeighbours(currentTriangle))
            {
                if (!closedSet.Contains(neighbour))
                {
                    float newCostToNeighbour = currentTriangle.gCost + GetDistanceEnd(neighbour, currentTriangle);

                    if (newCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                    {
                        neighbour.gCost = newCostToNeighbour;
                        neighbour.fCost = GetDistanceEnd(neighbour, end);

                        neighbour.chain = currentTriangle;
                        if (!openSet.Contains(neighbour))
                        {
                            openSet.Add(neighbour);
                        }
                    }
                }
            }
        }
        return foundPath;
    }

    private List<Triangle> RetracePath(Triangle start, Triangle end)
    {
        List<Triangle> path = new List<Triangle>();
        Triangle current = end;
        while (current.area != start.area)
        {
            path.Add(current);
            current = current.chain;
        }
        path.Add(start);
        path.Reverse();
        return path;
    }

    private List<Triangle> GetNeighbours(Triangle triangle)
    {
        
        List<Triangle> tList = new List<Triangle>();
        bool sameA = false;
        bool sameB = false;
        bool sameC = false;
        foreach (Triangle auxT in triangleList)
        {
            sameA = Tools.sameVertex(mesh.vertices[mesh.indices[auxT.index]], triangle, mesh);
            sameB = Tools.sameVertex(mesh.vertices[mesh.indices[auxT.index+1]], triangle, mesh);
            sameC = Tools.sameVertex(mesh.vertices[mesh.indices[auxT.index+2]], triangle, mesh);
            if ((sameA && sameB && !sameC) || (sameA && !sameB && sameC) || (!sameA && sameB && sameC))
            {
               
                if (auxT != null)
                {
                    tList.Add(auxT);
                }
            }
        }
        return tList;
    }

    

    private float GetDistance(Triangle t1, Triangle t2)
    {
        float distX = Mathf.Abs(t1.refPoint.x - t2.refPoint.x);
        float distY = Mathf.Abs(t1.refPoint.y - t2.refPoint.y);
        float distZ = Mathf.Abs(t1.refPoint.z - t2.refPoint.z);
        return distX + distZ + distY;
        
    }
    private float GetDistanceEnd(Triangle t1, Triangle t2)
    {
        float final = 0;

        float distX = Mathf.Abs(t1.refPoint.x - t2.refPoint.x);
        float distY = Mathf.Abs(t1.refPoint.y - t2.refPoint.y);
        float distZ = Mathf.Abs(t1.refPoint.z - t2.refPoint.z);
        float barycentredist = distX + distZ + distY;

        distX = Mathf.Abs(mesh.vertices[mesh.indices[t1.index]].x - t2.refPoint.x);
        distY = Mathf.Abs(mesh.vertices[mesh.indices[t1.index]].y - t2.refPoint.y);
        distZ = Mathf.Abs(mesh.vertices[mesh.indices[t1.index]].z - t2.refPoint.z);

        float vertex1dist = distX + distZ + distY;

        distX = Mathf.Abs(mesh.vertices[mesh.indices[t1.index + 1]].x - t2.refPoint.x);
        distY = Mathf.Abs(mesh.vertices[mesh.indices[t1.index + 1]].y - t2.refPoint.y);
        distZ = Mathf.Abs(mesh.vertices[mesh.indices[t1.index + 1] ].z - t2.refPoint.z);

        float vertex2dist = distX + distZ + distY;

        distX = Mathf.Abs(mesh.vertices[mesh.indices[t1.index + 2]].x - t2.refPoint.x);
        distY = Mathf.Abs(mesh.vertices[mesh.indices[t1.index + 2]].y - t2.refPoint.y);
        distZ = Mathf.Abs(mesh.vertices[mesh.indices[t1.index + 2]].z - t2.refPoint.z);

        float vertex3dist = distX + distZ + distY;

        Vector3 mid12 = Vector3.Lerp(mesh.vertices[mesh.indices[t1.index]], mesh.vertices[mesh.indices[t1.index + 1]], 0.5f);
        distX = Mathf.Abs(mid12.x - t2.refPoint.x);
        distY = Mathf.Abs(mid12.y - t2.refPoint.y);
        distZ = Mathf.Abs(mid12.z - t2.refPoint.z);

        float vertexmid12dist = distX + distZ + distY;

        Vector3 mid13 = Vector3.Lerp(mesh.vertices[mesh.indices[t1.index]], mesh.vertices[mesh.indices[t1.index + 2]], 0.5f);

        distX = Mathf.Abs(mid13.x - t2.refPoint.x);
        distY = Mathf.Abs(mid13.y - t2.refPoint.y);
        distZ = Mathf.Abs(mid13.z - t2.refPoint.z);

        float vertexmid13dist = distX + distZ + distY;

        Vector3 mid23 = Vector3.Lerp(mesh.vertices[mesh.indices[t1.index + 1]], mesh.vertices[mesh.indices[t1.index + 2]], 0.5f);

        distX = Mathf.Abs(mid23.x - t2.refPoint.x);
        distY = Mathf.Abs(mid23.y - t2.refPoint.y);
        distZ = Mathf.Abs(mid23.z - t2.refPoint.z);

        float vertexmid23dist = distX + distZ + distY;

        final = Mathf.Min(barycentredist, vertex1dist, vertex2dist, vertex3dist, vertexmid12dist, vertexmid13dist, vertexmid23dist);

        if (final == vertex1dist) t1.refPoint.Set(mesh.vertices[mesh.indices[t1.index]].x, mesh.vertices[mesh.indices[t1.index]].y, mesh.vertices[mesh.indices[t1.index]].z);
        else if (final == vertex2dist) t1.refPoint.Set(mesh.vertices[mesh.indices[t1.index + 1]].x, mesh.vertices[mesh.indices[t1.index + 1]].y, mesh.vertices[mesh.indices[t1.index + 1]].z);
        else if(final == vertex3dist) t1.refPoint.Set(mesh.vertices[mesh.indices[t1.index+2]].x, mesh.vertices[mesh.indices[t1.index+2]].y, mesh.vertices[mesh.indices[t1.index+2]].z);
        else if(final == vertexmid12dist) t1.refPoint = mid12;
        else if(final == vertexmid13dist) t1.refPoint = mid13;
        else if(final == vertexmid23dist) t1.refPoint = mid23;
        
        return final;
    }
    
}